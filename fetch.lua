local function writeFile(p, d)
	local f, err = io.open(p, "w")
	if not f then error(err) end
	local ok, err = f:write(d)
	f:close()
	if not ok then error(err) end
end

local function treeFind(tree, keys)
	if type(keys) ~= "table" then
		keys = {keys}
	end
	
	local function any(k, ks)
		for _, x in ipairs(ks) do
			if x == k then return true end
		end
		return false
	end
	
	local function search(t)
		if type(t) ~= "table" then
			return nil
		end
		
		for k, v in pairs(t) do
			if any(k, keys) then
				coroutine.yield(v)
			else
				search(v)
			end
		end
		return nil
	end
	local function resume(t, v)
		local ok, nv = coroutine.resume(t, v)
		if not ok then error(nv) end
		return nv
	end
	
	return resume, coroutine.create(search), tree
end

local function progressTicks()
	local ticks = 0
	io.write("0%"):flush()
	return function(p)
		if p > 1 then p = 1 end
		local nt = math.floor(p * 50)
		while ticks < nt do
			ticks = ticks + 1
			if ticks % 5 == 0 then
				io.write(tostring(ticks*2)..'%'):flush()
			else
				io.write('.'):flush()
			end
		end
	end
end

local function initialData(page)
	local s, e = page:find('var ytInitialData = ')
	if not e then
		return nil, "JSON data (ytInitialData) not found on channel page"
	end
	return json.parse(page:sub(e+1)), nil
end

url.setCookies(os.getenv("COOKIES"))

local headers = {"User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36"}

if argTestURL then
	local req = url.get(argTestURL, headers):start()
	local r, err = req:wait()
	if err then
		error(err)
	end
	
	local s, e = r:find('var ytInitialData = ')
	io.write(r:sub(e+1))
--[[	local data = initialData(r)
	
	for v in treeFind(data, "gridVideoRenderer") do
		print(v.navigationEndpoint.watchEndpoint.videoId, v.title.simpleText)
	end]]
	return
end

local cpath = os.getenv("HOME").."/.yt/subs.lua"
local conf = loadfile(cpath, "t")
local _, err = conf()
if err then
	error(err)
end

local vids = {}
function parseChannel(ch, page)
	local data, err = initialData(page)
	if err then return err end
	local channelTitle = data.metadata.channelMetadataRenderer.title or "NO TITLE"
	for v in treeFind(data, "gridVideoRenderer") do
		local vid = {channel = ch, channelTitle = channelTitle,
			id = v.navigationEndpoint.watchEndpoint.videoId,
			title = v.title.simpleText,
			live = false}
		for _, o in ipairs(v.thumbnailOverlays) do
			local time = o.thumbnailOverlayTimeStatusRenderer 
			if time and time.style == "LIVE" then
				vid.live = true
			end
		end
		vids[vid.id] = vid
	end
end

function fetchChannels(channels)
	print("Retrieving "..#channels.." channels...")
	local progress = progressTicks()
	local ndone = 0
	for i, name in ipairs(channels) do
		local req = url.get("https://www.youtube.com/"..name, headers)
		req:onComplete(function(r)
			local err = parseChannel(name, r:result())
			if err then return err end
			ndone = ndone + 1
			progress(ndone / #channels)
		end):start()
	end
end

fetchChannels(channels)

for req, err in url.wait do
	if err then
		print(err)
	end
end
io.write('\n')

if argLive then
	for _, vid in pairs(vids) do
		if vid.live then
			io.write("\n")
			io.write("["..vid.channelTitle.."] ".."https://www.youtube.com/watch?v="..vid.id.."\n")
			io.write(vid.title.."\n")
		end
	end
end

