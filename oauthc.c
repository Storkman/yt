#define _DEFAULT_SOURCE
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <unistd.h>

#include "lua/src/lua.h"
#include "lua/src/lualib.h"
#include "lua/src/lauxlib.h"
#include <curl/curl.h>

#include "common.h"
#include "json.h"

#define REQBUF (4*1024*1024)
#define BUFLEN 2048
#define VERCODELEN 128

typedef struct oauth_conf {
	char *atoken, *rtoken;
	char *authep, *tokenep, *revokep;
	char *scopes;
	char *id, *secret;
} oauth_conf;

struct curlbuff {
        char b[REQBUF];
        int p;
};

int	authorize(oauth_conf *conf, int challenge, int force_port);
int	browser(const char *url);
void	encode(char *code);
int	empty(char *s);
int	genchallenge(char vercode[VERCODELEN+1]);
int	getcode(char *code, int sfd);
char*	json_string(lua_State *L, const char *field);
int	parsecode(char *code, char *buf);
int	parseport(char *s);
int	postreq(lua_State *L, char *url, char *form);
int	readconf(oauth_conf *conf, const char *path);
int	refresh(oauth_conf *conf);
int	revoke_token(oauth_conf *conf);
int	startauth(oauth_conf *conf, int port, char *challenge);
int	startlisten(int *sfd, int force_port);
size_t	write_callback(char *ptr, size_t size, size_t nm, void *buf);
int	writeconf(oauth_conf *conf, const char *path);

enum { EP_NONE, EP_YT, EP_TWITCH };

int
main(int argc, char* argv[])
{
	int err;
	char *id = NULL, *secret = NULL;
	char *auth_ep = NULL, *token_ep = NULL, *revoke_ep = NULL;
	char *authpath = NULL;
	char *scopes = NULL;
	
	int challenge = 1;    // RFC7636
	int defep = EP_NONE;
	int dorevoke = 0, doprint = 0;
	int dorefresh = 0, donotrefresh = 0;
	int force_port = 0;
	
	oauth_conf conf = {NULL,};
	
	char **arg = argv+1;
	while (*arg) {
		if (0) {}
		else if (strcmp(*arg, "-auth-ep") == 0)
			auth_ep = *++arg;
		else if (strcmp(*arg, "-f") == 0)
			authpath = *++arg;
		else if (strcmp(*arg, "-id") == 0)
			id = *++arg;
		else if (strcmp(*arg, "-nochallenge") == 0)
			challenge = 0;
		else if (strcmp(*arg, "-port") == 0)
			force_port = parseport(*++arg);
		else if (strcmp(*arg, "-print") == 0)
			doprint = 1;
		else if (strcmp(*arg, "-r") == 0)
			dorefresh = 1;
		else if (strcmp(*arg, "-revoke") == 0)
			dorevoke = 1;
		else if (strcmp(*arg, "-revoke-ep") == 0)
			revoke_ep = *++arg;
		else if (strcmp(*arg, "-scopes") == 0)
			scopes = *++arg;
		else if (strcmp(*arg, "-secret") == 0)
			secret = *++arg;
		else if (strcmp(*arg, "-token-ep") == 0)
			token_ep = *++arg;
		else if (strcmp(*arg, "-twitch") == 0)
			defep = EP_TWITCH;
		else if (strcmp(*arg, "-yt") == 0)
			defep = EP_YT;
		else if (strcmp(*arg, "-1") == 0)
			donotrefresh = 1;
		else
			die("unknown option '%s'\n", *arg);
			
		if (!*arg)
			break;
		arg++;
	}
	
	if (dorefresh && donotrefresh)
		die("both -1 and -r given\n");
	
	if (authpath) {
		err = readconf(&conf, authpath);
		if (err && errno != ENOENT)
			die("failed to read auth file:");
	}
	
	if (auth_ep)
		conf.authep = auth_ep;
	if (revoke_ep)
		conf.revokep = revoke_ep;
	if (token_ep)
		conf.tokenep = token_ep;
	
	switch (defep) {
	case EP_YT:
		conf.authep = strdup("https://accounts.google.com/o/oauth2/v2/auth");
		conf.tokenep = strdup("https://www.googleapis.com/oauth2/v4/token");
		conf.revokep = strdup("https://accounts.google.com/o/oauth2/revoke");
		break;
	case EP_TWITCH:
		conf.authep = strdup("https://id.twitch.tv/oauth2/authorize");
		conf.tokenep = strdup("https://id.twitch.tv/oauth2/token");
		conf.revokep = strdup("https://id.twitch.tv/oauth2/revoke");
		break;
	default:
		break;
	}
	
	if (dorevoke)
		return revoke_token(&conf);
	
	if (scopes)
		conf.scopes = scopes;
	if (id)
		conf.id = id;
	if (secret)
		conf.secret = secret;
	
	if (!donotrefresh) {
		if (!refresh(&conf))
			goto FINISH;
	}
	
	if (dorefresh)
		return 1;
	
	if (empty(conf.authep))
		die("no authentication endpoint specified\n");
	if (empty(conf.tokenep))
		die("no token endpoint specified\n");
	if (!conf.scopes)
		conf.scopes = strdup("");
	if (empty(conf.id))
		die("no client id specified\n");
	if (!conf.secret)
		die("no client secret specified\n");
	
	if (authorize(&conf, challenge, force_port))
		return 1;
	
FINISH:
	if (doprint)
		puts(conf.atoken);	
	if (!authpath)
		return 0;
	err = writeconf(&conf, authpath);
	if (err)
		die("failed to write auth file:");
	return 0;
}

int
authorize(oauth_conf *conf, int challenge, int force_port)
{
	int srv, srvport;
	char code[BUFLEN], vercode[VERCODELEN+1];
	srvport = startlisten(&srv, force_port);
	if (srv < 0 || srvport < 0)
		return 1;
	
	/* Generate a code verifier */
	if (challenge) {
		if (genchallenge(vercode)) {
			close(srv);
			return 1;
		}
	}
	
	/* Open a browser window with a request to the authorization endpoint */
	if (startauth(conf, srvport, challenge ? vercode : NULL))
		return 1;
	if (getcode(code, srv))
		return 1;
	
	/* Exchange authorization code for access token */
	{
		char form[BUFLEN];
		lua_State *L;
		int flen = snprintf(form, BUFLEN,
			"code=%s&client_id=%s&client_secret=%s&redirect_uri=http://localhost%%3A%d&grant_type=authorization_code",
			code, conf->id, conf->secret, srvport);
		if (flen >= BUFLEN) {
			fprintf(stderr, "token request body too long\n");
			return 1;
		}
		
		if (challenge) {
			char par[BUFLEN];
			int len = snprintf(par, BUFLEN, "&code_verifier=%s", vercode);
			if (flen + len >= BUFLEN) {
				fprintf(stderr, "token request body too long\n");
				return 1;
			}
			strcat(form, par);
		}
		
		L = luaL_newstate();
		if (!L)
			die("can't allocate Lua state (out of memory)\n");
		if (postreq(L, conf->tokenep, form))
			return 1;
		
		conf->atoken = json_string(L, "access_token");
		if (!conf->atoken) {
			fprintf(stderr, "access_token missing in token endpoint response\n");
			lua_close(L);
			return 1;
		}
		conf->rtoken = json_string(L, "refresh_token");
		lua_close(L);
	}
	return 0;
}

int
refresh(oauth_conf *conf)
{
	char form[BUFLEN];
	int len;
	lua_State *L;
	
	if (empty(conf->id))
		die("no client id specified\n");
	if (empty(conf->secret))
		die("no client secret specified\n");
	if (empty(conf->rtoken)) {
		fprintf(stderr, "no cached refresh token\n");
		return 1;
	}
	
	len = snprintf(form, BUFLEN, "refresh_token=%s&client_id=%s&client_secret=%s&grant_type=refresh_token",
		conf->rtoken, conf->id, conf->secret);
	if (len >= BUFLEN) {
		fprintf(stderr, "token refresh request body too long\n");
		return 1;
	}
	
	L = luaL_newstate();
	if (!L)
		die("can't allocate Lua state (out of memory)\n");
	if (postreq(L, conf->tokenep, form))
		return 1;
	conf->atoken = json_string(L, "access_token");
	lua_close(L);
	if (!conf->atoken) {
		fprintf(stderr, "access_token missing in token refresh response\n");
		return 1;
	}
	return 0;
}

int
revoke_token(oauth_conf *conf)
{
	char form[BUFLEN];
	char *err;
	int len;
	lua_State *L;
	
	if (empty(conf->atoken))
		die("no cached access token\n");
	
	len = snprintf(form, BUFLEN, "token=%s", conf->atoken);
	if (len >= BUFLEN)
		die("token revoke request body too long\n");
	
	L = luaL_newstate();
	if (!L)
		die("can't allocate Lua state (out of memory)\n");
	if (empty(conf->revokep))
		die("no revoke endpoint specified\n");
	if (postreq(L, conf->revokep, form))
		die("failed to revoke token\n");
	err = json_string(L, "error");
	if (err)
		die("failed to revoke token: %s\n", err);
	lua_close(L);
	return 0;
}

int
readconf(oauth_conf *conf, const char *path)
{
	char buf[BUFLEN];
	char *p, *e;
	int fd, i, len, err;
	char **field[] = {
		&conf->atoken, &conf->rtoken,
		&conf->authep, &conf->tokenep, &conf->revokep,
		&conf->scopes,
		&conf->id, &conf->secret};
	
	fd = open(path, O_RDONLY);
	if (fd < 0)
		return 1;
	
	len = readall(buf, BUFLEN, fd);
	if (len < 0) {
		err = errno;
		close(fd);
		errno = err;
		return 1;
	}
	close(fd);
	if (len + 1 > BUFLEN) {
		errno = EFBIG;
		return 1;
	}
	buf[len] = 0;
	
	p = buf;
	for (i = 0; i < sizeof(field)/sizeof(field[0]); i++) {
		if (!*p)
			break;
		for (e = p; *e && *e != '\n'; e++) {}
		*field[i] = strndup(p, e - p);
		if (!*e)
			break;
		p = e + 1;
	}
	return 0;
}

int
writeconf(oauth_conf *conf, const char *path)
{
	char **field[] = {
		&conf->atoken, &conf->rtoken,
		&conf->authep, &conf->tokenep, &conf->revokep,
		&conf->scopes,
		&conf->id, &conf->secret};
	char *nf;
	int i, n;
	FILE *f = fopen(path, "w");
	if (!f)
		return 1;
	for (i = 0; i < sizeof(field)/sizeof(field[0]); i++) {
		nf = *field[i];
		n = fprintf(f, "%s\n", nf ? nf : "");
		if (n < 0) {
			fclose(f);
			return 1;
		}
	}
	fclose(f);
	return 0;
}

int
startauth(oauth_conf *conf, int port, char *challenge)
{
	int len;
	char verargs[BUFLEN], url[BUFLEN];
	verargs[0] = 0;
	
	if (challenge) {
		strcat(verargs, "&code_challenge_method=plain&code_challenge=");
		strcat(verargs, challenge);
	}
	
	len = snprintf(url, BUFLEN,
		"%s?client_id=%s&redirect_uri=http://localhost:%d&response_type=code&scope=%s%s",
		conf->authep, conf->id, port, conf->scopes, verargs);
	if (len >= BUFLEN) {
		fprintf(stderr, "authorization query too long\n");
		return 1;
	}
	
	return browser(url);
}

int
browser(const char *url)
{
	int pid, wstat;
	const char *browser = "xdg-open", *browserpref;
	browserpref = getenv("BROWSER");
	if (browserpref)
		browser = browserpref;
	pid = spawn1(browser, url);
	while (waitpid(pid, &wstat, 0) < 0) {}
	return WEXITSTATUS(wstat);
}

int
postreq(lua_State *L, char *url, char *form)
{
	char errb[CURL_ERROR_SIZE];
	const char *err;
	struct curlbuff *dst;
	CURL *c = curl_easy_init();
	dst = calloc(1, sizeof(*dst));
	if (!dst) {
		perror("failed to allocate request response buffer");
		return 1;
	}
	if (!c) {
		fprintf(stderr, "failed to create libcurl handle\n");
		return 1;
	}
	
	curl_easy_setopt(c, CURLOPT_URL, url);
	curl_easy_setopt(c, CURLOPT_POSTFIELDSIZE, strlen(form));
	curl_easy_setopt(c, CURLOPT_POSTFIELDS, form);
	curl_easy_setopt(c, CURLOPT_ERRORBUFFER, errb);
	curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(c, CURLOPT_WRITEDATA, dst);
	curl_easy_setopt(c, CURLOPT_ACCEPT_ENCODING, "");
	if (curl_easy_perform(c)) {
		curl_easy_cleanup(c);
		free(dst);
		fprintf(stderr, "request failed: %s\n", errb);
		return 1;
	}
	curl_easy_cleanup(c);
	
	err = json_parse(L, dst->b, dst->p);
	free(dst);
	if (err) {
		fprintf(stderr, "failed to parse response: %s\n", err);
		return 1;
	}
	return 0;
}

int
genchallenge(char vercode[VERCODELEN+1])
{
	int len;
	FILE *rand = fopen("/dev/urandom", "r");
	if (!rand) {
		perror("failed to open urandom");
		return 1;
	}
	len = fread(vercode, 1, VERCODELEN, rand);
	if (len < VERCODELEN) {
		if (feof(rand)) {
			fprintf(stderr, "failed to read from urandom: EOF\n");
			fclose(rand);
			return 1;
		}
		perror("failed to read from urandom");
		fclose(rand);
		return 1;
	}
	encode(vercode);
	vercode[128] = 0;
	fclose(rand);
	return 0;
}

void
encode(char *code)
{
	const char *tab = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-_";
	int i;
	for (i = 0; i < VERCODELEN; i++)
		code[i] = tab[code[i] & 63];
}

int
empty(char *s)
{
	return !s || strlen(s) == 0;
}

int
startlisten(int *sfd, int force_port)
{
	int err = 0, fd;
	socklen_t len;
	int yes = 1;
	struct sockaddr_in addr = {
		.sin_family = AF_INET,
		.sin_addr = {INADDR_ANY},
		.sin_port = htons(force_port),};
	fd = socket(AF_INET, SOCK_STREAM, 0);
	if (fd < 0) {
		perror("failed to create socket");
		return -1;
	}
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
	err = bind(fd, (struct sockaddr*) &addr, sizeof(addr));
	if (err) {
		perror("failed to bind socket");
		goto END;
	}
	err = listen(fd, 1);
	if (err) {
		perror("failed to listen on socket");
		goto END;
	}
	len = sizeof(addr);
	err = getsockname(fd, (struct sockaddr*) &addr, &len);
	if (err) {
		perror("failed to get socket address");
		goto END;
	}
END:
	if (err) {
		close(fd);
		return -1;
	}
	*sfd = fd;
	return ntohs(addr.sin_port);
}

size_t
write_callback(char *ptr, size_t size, size_t nm, void *ud)
{
	struct curlbuff *buf = ud;
	int nb = sizeof(buf->b) - buf->p;
	if (nm < nb)
		nb = nm;
	memmove(buf->b + buf->p, ptr, nb);
	buf->p += nb;
	return nb;
}

int
getcode(char *code, int sfd)
{
	int err = -1;
	FILE *f;
	char buf[BUFLEN];
	const char *resp = "You may now close this tab.\n";
	int fd = accept(sfd, NULL, NULL);
	if (fd < 0) {
		perror("failed to accept connection");
		close(sfd);
		return -1;
	}
	
	f = fdopen(fd, "a+");
	if (!f) {
		perror("fdopen failed");
		close(fd);
		close(sfd);
		return -1;
	}
	
	if (!fgets(buf, BUFLEN, f)) {
		perror("failed to read code");
		goto END;
	}
	
	err = parsecode(code, buf);
	if (err)
		goto END;
	
	for (;;) {
		if (!fgets(buf, BUFLEN, f))
			goto END;
		if (feof(f))
			goto END;
		if (strcmp(buf, "\r\n"))
			break;
	}
	
	fputs("HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\n", f);
	fprintf(f, "Content-Length: %ld\r\n\r\n", strlen(resp));
	fputs(resp, f);
END:
	fclose(f);
	close(fd);
	close(sfd);
	return err;
}

int
parsecode(char *code, char *buf)
{
	char *e;
	if (strncmp(buf, "GET /?", 6) != 0)
		fprintf(stderr, "invalid request: %s\n", buf);
	buf += 6;
	
	while (*buf) {
		if (strncmp(buf, "code=", 5) == 0)
			break;
		while (*buf && *buf != ' ' && *buf != '&')
			buf++;
		if (*buf == '&')
			buf++;
		if (!*buf || *buf == ' ') {
			fprintf(stderr, "code not returned in query");
			return 1;
		}
	}
	
	buf += 5;
	e = buf;
	while (*e && *e != ' ' && *e != '&')
		e++;
	memmove(code, buf, e-buf);
	code[e-buf] = 0;
	return 0;
}

int
parseport(char *s)
{
	char *e;
	int n = strtol(s, &e, 10);
	if (n < 0 || n > 65535 || *e != 0)
		die("invalid port number\n");
	return n;
}

char*
json_string(lua_State *L, const char *field)
{
	char *s;
	const char *ls;
	lua_getfield(L, -1, field);
	ls = lua_tostring(L, -1);
	if (!ls)
		return NULL;
	s = strdup(ls);
	lua_pop(L, 1);
	return s;
}
