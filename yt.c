#define _DEFAULT_SOURCE
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "lua/src/lua.h"
#include "lua/src/lualib.h"
#include "lua/src/lauxlib.h"

#include "common.h"
#include "json.h"

static int lua_fmtdur(lua_State *L);
static int lua_logtime(lua_State *L);
static int lua_mkdir(lua_State *L);
static int lua_parsedur(lua_State *L);
static int lua_spawn(lua_State *L);
static int lua_urlenc(lua_State *L);

static int urlencode(char *dst, int dlen, const char *src, int slen);

static struct luaL_Reg ytfuncs[] = {
	{ "duration",	lua_parsedur },
	{ "fmtduration",	lua_fmtdur },
	{ "logtime", 	lua_logtime },
	{ "mkdir", 	lua_mkdir },
	{ "spawn",	lua_spawn },
	{ "urlenc",	lua_urlenc },
	{ NULL,	NULL },
};

extern const char _binary_yt_luac_start[];
extern const char _binary_yt_luac_end[];

void
yt_init(lua_State *L)
{
	struct stat st;
	char *cookies;
	
	lua_createtable(L, 0, 3);
	luaL_setfuncs(L, ytfuncs, 0);
	
	cookies = getenv("COOKIES");
	if (cookies) {
		if (stat(cookies, &st))
			die("COOKIES=\"%s\" not accessible:", cookies);
		lua_pushstring(L, cookies);
		lua_setfield(L, -2, "cookies");
	}
	
	lua_setglobal(L, "yt");
	runembed(L, "ytboot.lua", _binary_yt_luac_start, _binary_yt_luac_end);
}

int
lua_urlenc(lua_State *L)
{
	size_t l;
	int rl;
	const char *src = luaL_checklstring(L, -1, &l);
	char *dst = malloc(l*3 + 1);
	if (!dst)
		return luaL_error(L, "can't urlencode: out of memory");
	rl = urlencode(dst, l*3 + 1, src, l);
	if (rl < 0)
		return luaL_error(L, "FIXME: urlencoded string doesn't fit into buffer");
	lua_pushlstring(L, dst, rl);
	return 1;
}

int
lua_parsedur(lua_State *L)
{
	size_t l;
	const char *s = luaL_checklstring(L, -1, &l);
	const char *os = s;
	char *e;
	int d, p, pn;
	int M = 0;
	const char *oo = "invalid duration format '%s': elements out of order", *idf = "invalid duration format '%s'";
	if (*s != 'P')
		return luaL_error(L, idf, os);
	s++;
	d = 0;
	pn = 0;
	while (*s) {
		if (*s == 'T') {
			pn = 2;
			s++;
			continue;
		}
		
		errno = 0;
		p = strtol(s, &e, 10);
		if (errno)
			return luaL_error(L, "invalid duration format '%s': %s", os, strerror(errno));
		if (s == e)
			return luaL_error(L, idf, os);
		s = e;
		switch (*e) {
		case 'D':
			if (pn > 0)
				return luaL_error(L, oo, os);
			d += 24*3600 * p;
			pn = 1;
			break;
		case 'H':
			if (pn != 2)
				return luaL_error(L, oo, os);
			d += 3600 * p;
			pn = 3;
			break;
		case 'M':
			if (pn < 2 || pn > 3 || M)
				return luaL_error(L, oo, os);
			d += 60 * p;
			pn = 4;
			M = 1;
			break;
		case 'S':
			if (pn < 2)
				return luaL_error(L, oo, os);
			d += p;
			pn = 5;
			break;
		default:
			return luaL_error(L, idf, os);
		}
		s++;
		if (pn == 5) {
			if (*s)
				return luaL_error(L, idf, os);
			break;
		}
	}
	lua_pushinteger(L, d);
	return 1;
}

int
lua_fmtdur(lua_State* L)
{
	char buf[256];
	int len;
	int h, m, s;
	int d = luaL_checkinteger(L, 1);
	s = d % 60;
	d /= 60;
	m = d % 60;
	h = d / 60;
	len = snprintf(buf, 256, "%d:%02d:%02d", h, m, s);
	lua_pushlstring(L, buf, len);
	return 1;
}

int
urlencode(char *dst, int dlen, const char *src, int slen)
{
	int di = 0, si;
	char reserved;
	const char *hex = "0123456789ABCDEF";
	for (si = 0; si < slen; si++) {
		switch (src[si]) {
		/*  !*'();:@&=+$,/?#[]  */
		case '!':	case '*':	case '\'':	case '(':	case ')':	case ';':
		case ':':	case '@':	case '&':	case '=':	case '+':	case '$':
		case ',':	case '/':	case '?':	case '#':	case '[':	case ']':
			reserved = 1;
			break;
		default:
			reserved = 0;
		}
		
		if (reserved || src[si] > '~' || src[si] < ' ') {
			if (di + 4 >= dlen) {
				dst[di] = 0;
				return -1;
			}
			dst[di++] = '%';
			dst[di++] = hex[(src[si] & 0xf0) >> 4];
			dst[di++] = hex[src[si] & 0x0f];
			continue;
		}
		
		if (di +1 >= dlen) {
			dst[di] = 0;
			return -1;
		}
		dst[di++] = src[si];
	}
	dst[di] = 0;
	return di;
}

int
lua_parse(lua_State *L)
{
	const char *s, *err;
	size_t len;
	s = lua_tolstring(L, 1, &len);
	err = json_parse(L, s, len);
	if (err)
		return luaL_error(L, "parse error: %s", err);
	return 1;
}

int
lua_logtime(lua_State *L)
{
	char tb[1024];
	size_t len;
	time_t now = time(NULL);
	struct tm ts;
	len = strftime(tb, sizeof(tb), "%y-%m-%d %H:%M:%S ", localtime_r(&now, &ts));
	strncpy(tb+len, tzname[ts.tm_isdst>0 ? 1 : 0], sizeof(tb) - len);
	lua_pushstring(L, tb);
	return 1;
}

#ifndef PATH_MAX
#define PATH_MAX 4096
#endif

int
lua_mkdir(lua_State *L)
{
	int err;
	size_t l;
	char buf[PATH_MAX];
	char *p;
	const char *arg = luaL_checklstring(L, 1, &l);
	
	if (l + 1 > sizeof(buf))
		return luaL_error(L, "mkdir: path too long");
	memmove(buf, arg, l+1);
	
	for (p = buf + (buf[0] == '/'); *p; p++) {
		if (*p != '/')
			continue;
		*p = 0;
		err = mkdir(buf, 0775);
		if (err && errno != EEXIST)
			return luaL_error(L, "mkdir: %s: %s", arg, strerror(errno));
		*p = '/';
	}
	err = mkdir(buf, 0775);
	if (err && errno != EEXIST)
		return luaL_error(L, "mkdir: %s: %s", arg, strerror(errno));
	return 0;
}

int
lua_spawn(lua_State *L)
{
	int i, len;
	char **args;
	while (waitpid(-1, NULL, WNOHANG) > 0) {}
	luaL_checktype(L, 1, LUA_TTABLE);
	lua_len(L, -1);
	len = lua_tointeger(L, -1);
	lua_pop(L, 1);
	
	args = malloc(sizeof(char*) * (len+1));
	if (!args)
		die("out of memory");
	for (i = 0; i < len; i++) {
		lua_geti(L, -1, i+1);
		args[i] = strdup(lua_tostring(L, -1));
		lua_pop(L, 1);
	}
	args[i] = NULL;
	
	lua_pushinteger(L, spawn(args, NULL));
	for (i = 0; i < len; i++)
		free(args[i]);
	free(args);
	return 1;
}
