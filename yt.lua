--[[
	yt.duration define in yt.c:/^lua_parsedur/
	yt.fmtduration defined in yt.c:/^lua_fmtdur/
	yt.logtime defined in yt.c:/^lua_logtime/
	yt.spawn defined at yt.c:/^lua_spawn/
	yt.urlenc defined in yt.c:/^lua_urlenc/
--]]

function show(v, out)
	out = out or io.stdout
	local t = type(v)
	if type(v) == "function" then
		out:write "<function>"
		return
	end
	
	if type(v) == "boolean" then
		if v then out:write("true") else out:write("false") end
		return
	end
	
	if type(v) ~= "table" then
		out:write(v or "nil")
		return
	end
	
	out:write "{"
	local first = true
	local shown = {}
	for k, e in ipairs(v) do
		if not first then out:write ', ' end
		first = false
		show(e, out)
		shown[k] = true
	end
	
	for k, e in pairs(v) do
		if not shown[k] then
			if not first then out:write ', ' end
			first = false
			show(k, out)
			out:write " = "
			show(e, out)
		end
	end
	out:write "}"
end
