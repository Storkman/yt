#define _DEFAULT_SOURCE
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>

#include "lua/src/lua.h"
#include "lua/src/lualib.h"
#include "lua/src/lauxlib.h"

#include "common.h"

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}

	exit(1);
}

void
loadembed(lua_State *L, const char *name, const char *start, const char *end)
{
	switch (luaL_loadbuffer(L, start, end - start, name)) {
	case LUA_ERRSYNTAX:
		die("Lua returned a syntax error while loading compiled boot code from %s. This should be impossible.", name);
		break;
	case LUA_ERRMEM:
		die("out of memory");
		break;
	case LUA_ERRGCMM:
		die("error while running a __gc metamethod");
		break;
	}
}

void
runembed(lua_State *L, const char *name, const char *start, const char *end)
{
	loadembed(L, name, start, end);
	lua_call(L, 0, 0);
}

int
readall(char *buf, int bs, int fd)
{
	int len, wlen = 0;
	while (wlen < bs) {
		len = read(fd, buf, bs - wlen);
		switch (len) {
		case -1:
			return -1;
		case 0:
			return wlen;
		default:
			wlen += len;
			buf += len;
		}
	}
	return wlen;
}

int
spawn(char *const arg[], int *outfd)
{
	int pid;
	int pf[2];
	
	if (outfd) {
		int err = pipe(pf);
		if (err) {
			perror("failed to create pipe");
			return -1;
		}
		*outfd = pf[0];
	}
	
	pid = fork();
	switch (pid) {
	case -1:
		perror("failed to fork");
		return 0;
	
	case 0:
		if (outfd) {
			close(pf[0]);
			dup2(pf[1], 1);
		}
		execvp(arg[0], arg);
		die("exec failed: %s:", arg[0]);
	default:
		if (outfd)
			close(pf[1]);
		return pid;
	}
}

int
spawn1(const char *prog, const char *arg)
{
	char *p = strdup(prog), *a = strdup(arg);
	char *const args[3] = {p, a, NULL};
	int pid = 0;
	if (!p || !a) {
		fprintf(stderr, "out of memory\n");
		goto END;
	}
	pid = spawn(args, NULL);
END:
	free(p);
	free(a);
	return pid;
}
